package de.schegge.hamcrest;

import static de.schegge.hamcrest.OptionalMatchers.hasValue;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertSame;

import com.google.testing.compile.Compilation;
import com.google.testing.compile.Compilation.Status;
import com.google.testing.compile.Compiler;
import com.google.testing.compile.JavaFileObjects;
import de.schegge.hamcrest.impl.HamcrestMatcherBuilderProcessor;
import java.io.IOException;
import java.util.List;
import javax.tools.JavaFileObject;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class HamcrestMatcherBuilderProcessorTest {

  @Test
  void withoutAnnotation() {
    Compilation compilation =
        Compiler.javac().withProcessors(new HamcrestMatcherBuilderProcessor())
            .compile(JavaFileObjects.forSourceString("HamcrestTest", "public class HamcrestTest {}"));
    assertSame(Status.SUCCESS, compilation.status());
  }

  @ParameterizedTest
  @CsvSource({
      "boolean,public static Matcher<Pojo> hasValue(final Matcher<Boolean> valueMatcher)",
      "Boolean,public static Matcher<Pojo> hasValue(final Matcher<Boolean> valueMatcher)",
      "boolean,public static Matcher<Pojo> hasValue(final Boolean value)",
      "Boolean,public static Matcher<Pojo> hasValue(final Boolean value)",
      "int,public static Matcher<Pojo> hasValue(final Matcher<Integer> valueMatcher)",
      "Integer,public static Matcher<Pojo> hasValue(final Matcher<Integer> valueMatcher)",
      "long,public static Matcher<Pojo> hasValue(final Matcher<Long> valueMatcher)",
      "Long,public static Matcher<Pojo> hasValue(final Matcher<Long> valueMatcher)",
      "float,public static Matcher<Pojo> hasValue(final Matcher<Float> valueMatcher)",
      "Float,public static Matcher<Pojo> hasValue(final Matcher<Float> valueMatcher)",
      "double,public static Matcher<Pojo> hasValue(final Matcher<Double> valueMatcher)",
      "Double,public static Matcher<Pojo> hasValue(final Matcher<Double> valueMatcher)",
      "Object,public static Matcher<Pojo> hasValue(final Matcher<Object> valueMatcher)",
      "String,public static Matcher<Pojo> hasValue(final Matcher<String> valueMatcher)",
      "java.util.Optional<String>,public static Matcher<Pojo> hasOptionalValue(final Matcher<Optional<String>> valueMatcher)",
      "java.util.Optional<Number>,public static Matcher<Pojo> hasValue(final Matcher<Number> valueMatcher)",
      "java.util.Optional<String>,public static Matcher<Pojo> hasEmptyValue()",
  })
  void withHamcrestAnnotation(String sourceType, String result) {
    String sourcePojoTemplate = "package de.schegge;"
        + "public class Pojo { private %1$s value; public %1$s getValue() { return value; }}";
    JavaFileObject pojoSource = JavaFileObjects.forSourceString("de.schegge.Pojo", String.format(sourcePojoTemplate, sourceType));

    JavaFileObject hamcrestTestSource = JavaFileObjects.forSourceString("de.schegge.HamcrestTest",
            """
                    package de.schegge;
                    import de.schegge.hamcrest.annotations.Hamcrest;
                    @Hamcrest(Pojo.class)
                    class HamcrestTest {
                    }
                    """);
    Compilation compilation = Compiler.javac().withProcessors(new HamcrestMatcherBuilderProcessor())
        .compile(List.of(pojoSource, hamcrestTestSource));
    compilation.errors().forEach(System.out::println);
    compilation.diagnostics().forEach(System.out::println);
    compilation.generatedSourceFiles().forEach(s -> System.out.println(s.getName()));
    compilation.generatedSourceFiles().stream().map(x -> {
        try {
            return x.getCharContent(true);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }).forEach(c -> System.out.println("SOURCE: " + c));
    assertSame(Status.SUCCESS, compilation.status());
    assertThat(compilation.generatedSourceFile("de.schegge.PojoMatchers").map(this::getContent), hasValue(containsString(result)));
  }

  @Test
  void fromWildlife() {
    JavaFileObject pojoSource = JavaFileObjects.forSourceString("de.schegge.enumconverter.ValueHolder", """
                    package de.schegge.enumconverter;
                    
                    import java.util.List;
                    
                    public final class ValueHolder {
                    
                      private final String key;
                      private final List<String> value;
                    
                      private final boolean plain;
                    
                      public ValueHolder(String key, List<String> value, boolean plain) {
                        this.key = key;
                        this.value = value;
                        this.plain = plain;
                      }
                    
                      public String getKey() {
                        return key;
                      }
                    
                      public List<String> getValue() {
                        return value;
                      }
                    
                      public boolean isPlain() {
                        return plain;
                      }
                    
                      @Override
                      public String toString() {
                        return key + "=" + value + " (" + plain + ")";
                      }
                    }
                    """);

    JavaFileObject hamcrestTestSource = JavaFileObjects.forSourceString("de.schegge.HamcrestTest",
            """
                    package de.schegge;
                    import de.schegge.hamcrest.annotations.Hamcrest;
                    import de.schegge.enumconverter.ValueHolder;
                    @Hamcrest(ValueHolder.class)
                    class HamcrestTest {}
                    """);
    Compilation compilation = Compiler.javac().withProcessors(new HamcrestMatcherBuilderProcessor()).compile(List.of(pojoSource, hamcrestTestSource));
    compilation.errors().forEach(System.out::println);
    compilation.diagnostics().forEach(System.out::println);
    compilation.generatedSourceFiles().forEach(s -> System.out.println(s.getName()));
    compilation.generatedSourceFiles().stream().map(this::getContent).forEach(c -> System.out.println("SOURCE: " + c));
    assertSame(Status.SUCCESS, compilation.status());
  }

  private String getContent(JavaFileObject x) {
    try {
      return (String) x.getCharContent(true);
    } catch (IOException e) {
      return null;
    }
  }
}
