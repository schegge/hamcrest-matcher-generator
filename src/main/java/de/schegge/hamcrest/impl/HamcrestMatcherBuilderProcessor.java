package de.schegge.hamcrest.impl;

import static java.util.stream.Collectors.toList;
import static javax.lang.model.util.ElementFilter.typesIn;

import com.google.auto.service.AutoService;
import de.schegge.hamcrest.annotations.Hamcrest;
import de.schegge.hamcrest.annotations.Hamcrests;
import de.schegge.processor.SourceCodeGenerator;
import de.schegge.processor.TypeDictionary;
import de.schegge.processor.TypeDictionaryBuilder;
import de.schegge.processor.visitors.AbstractAnnotationValueVisitor;
import de.schegge.processor.visitors.AbstractElementVisitor;
import de.schegge.processor.visitors.AnnotationAnnotationValueVisitor;
import de.schegge.processor.visitors.AnnotationTypeValueVisitor;
import de.schegge.processor.visitors.TypesFromGenericTypeCollector;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;
import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.PrimitiveType;
import javax.lang.model.type.TypeMirror;
import javax.tools.Diagnostic.Kind;
import javax.tools.JavaFileObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SupportedAnnotationTypes({"de.schegge.hamcrest.annotations.Hamcrest", "de.schegge.hamcrest.annotations.Hamcrests"})
@SupportedSourceVersion(SourceVersion.RELEASE_21)
@AutoService(Processor.class)
public class HamcrestMatcherBuilderProcessor extends AbstractProcessor {

  private static final Logger LOGGER = LoggerFactory.getLogger(HamcrestMatcherBuilderProcessor.class);

  public static final Set<Modifier> FORBIDDEN_MODIFIERS = Set.of(Modifier.ABSTRACT, Modifier.PRIVATE, Modifier.STATIC);

  public static final TypesFromGenericTypeCollector TYPES_FROM_GENERIC_TYPE_COLLECTOR = new TypesFromGenericTypeCollector();

  private TypeElement hamcrests;
  private TypeElement hamcrest;

  private AnnotationValueExtractor<TypeMirror> hamcrestSourceTypeExtractor;
  private AnnotationValueExtractor<List<TypeMirror>> hamcrestsSourceTypeExtractor;

  private SourceCodeGenerator classGenerator;

  @Override
  public synchronized void init(ProcessingEnvironment processingEnv) {
    super.init(processingEnv);
    classGenerator = new SourceCodeGenerator();
    Name name = processingEnv.getElementUtils().getName("value");

    hamcrests = processingEnv.getElementUtils().getTypeElement(Hamcrests.class.getName());
    ExecutableElement hamcrestsValue = getAnnotationAttribute(name, hamcrests);

    hamcrest = processingEnv.getElementUtils().getTypeElement(Hamcrest.class.getName());
    ExecutableElement hamcrestValue = getAnnotationAttribute(name, hamcrest);

    hamcrestSourceTypeExtractor = new AnnotationValueExtractor<>(hamcrestValue, new AnnotationTypeValueVisitor());
    hamcrestsSourceTypeExtractor = new AnnotationValueExtractor<>(hamcrestsValue,
        new AbstractAnnotationValueVisitor<>() {
          @Override
          public List<TypeMirror> visitArray(List<? extends AnnotationValue> vals, Void unused) {
            return vals.stream()
                .map(val -> val.accept(new AnnotationAnnotationValueVisitor(), null))
                .map(a -> a.getElementValues().get(hamcrestValue))
                .map(a -> a.accept(new AnnotationTypeValueVisitor(), null))
                .collect(toList());
          }
        });
  }

  private ExecutableElement getAnnotationAttribute(Name name, TypeElement hamcrest) {
    return processingEnv.getElementUtils().getAllMembers(hamcrest).stream()
        .map(m -> m.accept(new AbstractElementVisitor<ExecutableElement, Void>() {
          @Override
          public ExecutableElement visitExecutable(ExecutableElement e, Void ignored) {
            return name.equals(e.getSimpleName()) ? e : null;
          }
        }, null)).filter(Objects::nonNull).findFirst().orElseThrow();
  }

  private record TestHolder(TypeElement type, TypeMirror asserted) {}

  @Override
  public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
    if (annotations.isEmpty()) {
      return false;
    }
    Stream<TestHolder> hamcrestsSourceTypes = annotations.stream().filter(hamcrests::equals)
        .flatMap(h -> processHamcrestsAnnotations(h, roundEnv));
    Stream<TestHolder> hamcrestSourceTypes = annotations.stream().filter(hamcrest::equals)
        .flatMap(h -> processHamcrestAnnotations(h, roundEnv));
    return Stream.concat(hamcrestSourceTypes, hamcrestsSourceTypes).distinct()
        .filter(Objects::nonNull).allMatch(this::processSourceTypes);
  }

  private Stream<TestHolder> processHamcrestsAnnotations(TypeElement annotation, RoundEnvironment roundEnv) {
    Set<TypeElement> typeElements = typesIn(roundEnv.getElementsAnnotatedWith(annotation));
    List<TestHolder> testHolders = new ArrayList<>();
    for (TypeElement typeElement : typeElements) {
      testHolders.addAll(typeElement.getAnnotationMirrors().stream()
              .filter(m -> m.getAnnotationType().asElement().equals(hamcrests))
              .map(m -> m.getAnnotationType().asElement()
                      .accept(hamcrestsSourceTypeExtractor, processingEnv.getElementUtils().getElementValuesWithDefaults(m)))
              .flatMap(List::stream).map(t -> new TestHolder(typeElement, t)).toList());
    }
    return testHolders.stream();
  }

  private Stream<TestHolder> processHamcrestAnnotations(TypeElement annotation, RoundEnvironment roundEnv) {
    Set<TypeElement> typeElements = typesIn(roundEnv.getElementsAnnotatedWith(annotation));
    List<TestHolder> testHolders = new ArrayList<>();
    for (TypeElement typeElement : typeElements) {
      testHolders.addAll(typeElement.getAnnotationMirrors().stream()
              .filter(m -> m.getAnnotationType().asElement().equals(hamcrest))
              .map(m -> m.getAnnotationType().asElement()
                      .accept(hamcrestSourceTypeExtractor, processingEnv.getElementUtils().getElementValuesWithDefaults(m)))
              .map(t -> new TestHolder(typeElement, t)).toList());
    }
    return testHolders.stream();
  }

  private boolean processSourceTypes(TestHolder type) {
    TypeElement sourceType = (TypeElement) processingEnv.getTypeUtils().asElement(type.asserted());
    if (!sourceType.getTypeParameters().isEmpty()) {
      LOGGER.warn("parameterized source types are not supported: {}", type);
      return false;
    }
    List<ExecutableElement> methods = sourceType.getEnclosedElements().stream()
        .filter(element -> element.getKind() == ElementKind.METHOD)
        .map(ExecutableElement.class::cast)
        .filter(this::filterGetter)
        .collect(toList());
    processingEnv.getMessager().printMessage(Kind.NOTE, "Methods: " + methods);
    try {
      return createMatcherClassFile(methods, sourceType, type.type());
    } catch (IOException ex) {
      LOGGER.warn("Error {}", ex.getMessage(), ex);
      return false;
    }
  }

  private boolean filterGetter(ExecutableElement method) {
    Set<Modifier> modifiers = method.getModifiers();
    if (FORBIDDEN_MODIFIERS.stream().anyMatch(modifiers::contains)) {
      return false;
    }
    if (!method.getParameters().isEmpty()) {
      return false;
    }
    String name = method.getSimpleName().toString();
    if ("getClass".equals(name)) {
      return false;
    }
    return name.startsWith("is") && name.length() > 2 || name.startsWith("get") && name.length() > 3;
  }

  private boolean createMatcherClassFile(List<ExecutableElement> methods, TypeElement sourceType, TypeElement testType) throws IOException {
    String packageName = testType.getEnclosingElement().accept(new PackageFinder(), null);
    String matcherTypeName = packageName + "." + sourceType.getSimpleName().toString() + "Matchers";

    JavaFileObject builderFile = processingEnv.getFiler().createSourceFile(matcherTypeName);
    if (builderFile.getLastModified() > 0) {
      LOGGER.debug("Ignore Java File: {}", builderFile.toUri());
      return false;
    }
    LOGGER.debug("Create Java File: {}", builderFile.toUri());
    String sourceName = sourceType.getSimpleName().toString();
    List<BasicMethodTemplate> methodTemplates = methods.stream()
        .map(method -> createMatcherMethod(sourceName, method)).flatMap(List::stream).toList();

    TypeDictionaryBuilder dictionaryBuilder = TypeDictionaryBuilder.of(processingEnv, sourceType);
    methodTemplates.stream().map(BasicMethodTemplate::getReturnTypeMirror).map(t ->
        t.accept(TYPES_FROM_GENERIC_TYPE_COLLECTOR, null)).forEach(dictionaryBuilder::add);
    TypeDictionary typeDictionary = dictionaryBuilder.create();

    List<WrapperMethodTemplate> wrapperMethodTemplates = methodTemplates.stream().map(mt -> {
      String newReturnType = mt.getReturnTypeMirror().accept(typeDictionary.getTypeNameGenerator(), null);
      return new WrapperMethodTemplate(mt, newReturnType);
    }).collect(toList());

    try (PrintWriter out = new PrintWriter(builderFile.openWriter())) {
      processTemplate(sourceType, typeDictionary.getBasePackageName(), out, wrapperMethodTemplates, typeDictionary.getImports());
    }
    return true;
  }

  void processTemplate(TypeElement sourceType, String packageName, PrintWriter out,
      List<? extends MethodTemplate> methodTemplates, List<String> imports) {
    String sourceTypeName = sourceType.getSimpleName().toString();
    Map<String, Object> root = new HashMap<>();
    root.put("package", packageName);
    root.put("sourceType", sourceTypeName);
    root.put("matcherTypeName", sourceTypeName + "Matchers");
    root.put("methods", methodTemplates);
    root.put("imports", imports);
    classGenerator.processTemplate(root, out);
  }

  private List<BasicMethodTemplate> createMatcherMethod(String sourceName, ExecutableElement method) {
    String methodName = method.getSimpleName().toString();
    String attributeName = createArgumentName(methodName);
    TypeMirror returnType = method.getReturnType();
    if (returnType instanceof PrimitiveType) {
      return List.of(new BasicMethodTemplate("class.method", sourceName, attributeName,
          processingEnv.getTypeUtils().boxedClass((PrimitiveType) returnType).asType(), methodName, null));
    }
    DeclaredType declaredType = (DeclaredType) returnType;
    List<? extends TypeMirror> typeArguments = declaredType.getTypeArguments();
    TypeElement typeElement = (TypeElement) declaredType.asElement();
    if (typeElement.toString().equals(Optional.class.getName())) {
      TypeMirror returnTypeParameter = typeArguments.getFirst();
      String returnTypeName = returnTypeParameter.toString();
      LOGGER.debug("Optional Return Type Name: {}", returnTypeName);
      return List.of(
          new BasicMethodTemplate("class.optionalMatcherMethod", sourceName, attributeName,
              declaredType, methodName, returnTypeName),
          new BasicMethodTemplate("class.optionalMethod", sourceName, attributeName,
              returnTypeParameter, methodName, returnTypeName));
    }
    String genericParameter = typeArguments.isEmpty() ? null : typeArguments.getFirst().toString();
    return List.of(
        new BasicMethodTemplate("class.method", sourceName, attributeName, declaredType, methodName,
            genericParameter));
  }

  private String createArgumentName(String methodName) {
    String attributeName1 = methodName.startsWith("get") ? methodName.substring(3) : null;
    String attributeName2 = methodName.startsWith("is") ? methodName.substring(2) : null;
    return Objects.requireNonNullElse(attributeName1, attributeName2);
  }
}