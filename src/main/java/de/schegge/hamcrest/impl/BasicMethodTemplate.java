package de.schegge.hamcrest.impl;

import javax.lang.model.type.TypeMirror;

public final class BasicMethodTemplate implements MethodTemplate {

  private final String sourceType;
  private final String template;
  private final String methodName;
  private final String attributeName;
  private final String variableName;

  private final TypeMirror returnTypeMirror;

  @Override
  public String getGenericParameter() {
    return genericParameter;
  }

  private final String genericParameter;

  public BasicMethodTemplate(String template, String source, String attributeName,
      TypeMirror returnTypeMirror, String methodName, String genericParameter) {
    this.template = template;
    sourceType = source;
    this.attributeName = attributeName;
    this.returnTypeMirror = returnTypeMirror;
    this.methodName = methodName;
    this.genericParameter = genericParameter;
    variableName = attributeName.substring(0, 1).toLowerCase() + attributeName.substring(1);
  }

  @Override
  public String getSourceType() {
    return sourceType;
  }

  @Override
  public String getTemplate() {
    return template;
  }

  @Override
  public String getMethodName() {
    return methodName;
  }

  @Override
  public String getReturnType() {
    return returnTypeMirror.toString();
  }

  @Override
  public String getAttributeName() {
    return attributeName;
  }

  @Override
  public String getVariableName() {
    return variableName;
  }

  public TypeMirror getReturnTypeMirror() {
    return returnTypeMirror;
  }
}
