package de.schegge.hamcrest.impl;

import de.schegge.processor.visitors.AbstractAnnotationValueVisitor;
import de.schegge.processor.visitors.AbstractElementVisitor;
import java.util.Map;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;

public class AnnotationValueExtractor<R> extends
    AbstractElementVisitor<R, Map<? extends ExecutableElement, ? extends AnnotationValue>> {

  private final ExecutableElement annotationAttribute;
  private final AbstractAnnotationValueVisitor<R, Void> valueFetcher;

  public AnnotationValueExtractor(ExecutableElement annotationAttribute,
      AbstractAnnotationValueVisitor<R, Void> valueFetcher) {
    this.annotationAttribute = annotationAttribute;
    this.valueFetcher = valueFetcher;
  }

  @Override
  public R visitType(TypeElement e,
      Map<? extends ExecutableElement, ? extends AnnotationValue> elementValues) {
    return e.getEnclosedElements().stream().map(enclosed -> enclosed.accept(this, elementValues)).findFirst()
        .orElse(null);
  }

  @Override
  public R visitExecutable(ExecutableElement e,
      Map<? extends ExecutableElement, ? extends AnnotationValue> elementValues) {
    return annotationAttribute.equals(e) ? elementValues.get(e).accept(valueFetcher, null) : null;
  }
}
