package de.schegge.hamcrest.impl;

public class WrapperMethodTemplate implements MethodTemplate {

  private final MethodTemplate wrapped;
  private final String returnType;

  public WrapperMethodTemplate(MethodTemplate wrapped, String returnType) {
    this.wrapped = wrapped;
    this.returnType = returnType;
  }

  @Override
  public String getGenericParameter() {
    return wrapped.getGenericParameter();
  }

  @Override
  public String getSourceType() {
    return wrapped.getSourceType();
  }

  @Override
  public String getTemplate() {
    return wrapped.getTemplate();
  }

  @Override
  public String getMethodName() {
    return wrapped.getMethodName();
  }

  @Override
  public String getReturnType() {
    return returnType;
  }

  @Override
  public String getAttributeName() {
    return wrapped.getAttributeName();
  }

  @Override
  public String getVariableName() {
    return wrapped.getVariableName();
  }
}
