package de.schegge.hamcrest.impl;

public interface MethodTemplate {

  String getGenericParameter();

  String getSourceType();

  String getTemplate();

  String getMethodName();

  String getReturnType();

  String getAttributeName();

  String getVariableName();
}
