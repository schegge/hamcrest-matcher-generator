package de.schegge.hamcrest.impl;

import javax.lang.model.element.Element;
import javax.lang.model.element.ElementVisitor;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.TypeParameterElement;
import javax.lang.model.element.VariableElement;

public class PackageFinder implements ElementVisitor<String, Void> {
    @Override
    public String visit(Element e, Void unused) {
        return "";
    }

    @Override
    public String visitPackage(PackageElement e, Void unused) {
        return e.getQualifiedName().toString();
    }

    @Override
    public String visitType(TypeElement e, Void unused) {
        return e.getEnclosingElement().accept(this, null);
    }

    @Override
    public String visitVariable(VariableElement e, Void unused) {
        return "";
    }

    @Override
    public String visitExecutable(ExecutableElement e, Void unused) {
        return "";
    }

    @Override
    public String visitTypeParameter(TypeParameterElement e, Void unused) {
        return "";
    }

    @Override
    public String visitUnknown(Element e, Void unused) {
        return "";
    }
}
