package de.schegge.processor;

import static java.util.stream.Collectors.toList;

import de.schegge.processor.visitors.AbstractTypeVisitor;
import de.schegge.processor.visitors.DictionaryBasedTypeNameGenerator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.annotation.processing.ProcessingEnvironment;
import javax.tools.Diagnostic.Kind;

public class TypeDictionary {

  private final ProcessingEnvironment processingEnv;
  private final String basePackageName;
  private final Map<String, String> dictionary;
  private final List<String> imports;

  public TypeDictionary(ProcessingEnvironment processingEnv, String basePackageName, Map<String, String> dictionary) {
    this.processingEnv = processingEnv;
    this.basePackageName = basePackageName;
    this.dictionary = dictionary;
    imports = createImports();
  }

  public String getBasePackageName() {
    return basePackageName;
  }

  public Map<String, String> getDictionary() {
    return dictionary;
  }

  public List<String> getImports() {
    return imports;
  }

  public AbstractTypeVisitor<String, Map<String, String>> getTypeNameGenerator() {
    return new DictionaryBasedTypeNameGenerator(processingEnv, dictionary);
  }

  private List<String> createImports() {
    List<String> result = getDictionary().entrySet().stream()
        .filter(e -> !e.getKey().equals(e.getValue()))
        .filter(e -> !e.getKey().equals("java.lang." + e.getValue()))
        .filter(e -> !e.getKey().equals(basePackageName + "." + e.getValue()))
        .map(Entry::getKey).collect(toList());
    processingEnv.getMessager().printMessage(Kind.NOTE, "result: " + result);
    return result;
  }
}
