package de.schegge.processor;

import static java.util.stream.Collectors.groupingBy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.tools.Diagnostic.Kind;

public class TypeDictionaryBuilder {

  private final ProcessingEnvironment processingEnv;
  private final Set<TypeMirror> types;
  private final String basePackageName;

  public TypeDictionaryBuilder(ProcessingEnvironment processingEnv,
      Set<TypeMirror> types, String basePackageName) {
    this.processingEnv = processingEnv;
    this.types = types;
    this.basePackageName = basePackageName;
  }

  public static TypeDictionaryBuilder of(ProcessingEnvironment processingEnv, TypeElement sourceType) {
    String basePackageName = processingEnv.getElementUtils().getPackageOf(sourceType).getQualifiedName().toString();
    return new TypeDictionaryBuilder(processingEnv, new HashSet<>(Set.of(sourceType.asType())),
        basePackageName);
  }

  public void add(Set<TypeMirror> types) {
    this.types.addAll(types);
    processingEnv.getMessager().printMessage(Kind.NOTE, "add types: " + types);
  }

  public TypeDictionary create() {
    Map<String, List<TypeElement>> packageMap = createPackageMap();
    return new TypeDictionary(processingEnv, basePackageName, createTypeDictionary(packageMap));
  }

  private Map<String, String> createTypeDictionary(Map<String, List<TypeElement>> packageMap) {
    processingEnv.getMessager().printMessage(Kind.NOTE, "package map:  " + packageMap);
    processingEnv.getMessager().printMessage(Kind.NOTE, "base package: " + basePackageName);

    List<String> packageList = sortPackageList(basePackageName, packageMap.keySet());

    Map<String, String> typeDictionary = new HashMap<>();
    Set<String> importSet = new HashSet<>();
    packageList.stream().map(packageMap::get).flatMap(List::stream).filter(Objects::nonNull).forEach(tm -> {
      String simpleName = tm.getSimpleName().toString();
      String key = processingEnv.getTypeUtils().erasure(tm.asType()).toString();
      if (importSet.add(simpleName)) {
        typeDictionary.put(key, simpleName);
      } else if (!simpleName.equals(typeDictionary.get(key))) {
        typeDictionary.put(key, tm.toString());
      }
    });
    processingEnv.getMessager().printMessage(Kind.NOTE, "import set:      " + importSet);
    processingEnv.getMessager().printMessage(Kind.NOTE, "type dictionary: " + typeDictionary);
    return typeDictionary;
  }

  private List<String> sortPackageList(String packageString, Set<String> packages) {
    List<String> packageList = new ArrayList<>(packages);
    packageList.removeAll(List.of(packageString, "java.lang"));
    packageList.addFirst("java.lang");
    packageList.addFirst(packageString);
    return packageList;
  }

  private Map<String, List<TypeElement>> createPackageMap() {
    return types.stream()
        .filter(t -> t.getKind() == TypeKind.DECLARED)
        .map(t -> processingEnv.getTypeUtils().erasure(t))
        .map(t -> (TypeElement) processingEnv.getTypeUtils().asElement(t))
        .collect(groupingBy(e -> processingEnv.getElementUtils().getPackageOf(e).toString()));
  }
}
