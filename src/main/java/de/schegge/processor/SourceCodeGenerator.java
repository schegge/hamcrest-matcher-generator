package de.schegge.processor;

import org.freshmarker.Configuration;
import org.freshmarker.Template;
import org.freshmarker.core.plugin.BooleanPluginProvider;
import org.freshmarker.core.plugin.DatePluginProvider;
import org.freshmarker.core.plugin.EnumPluginProvider;
import org.freshmarker.core.plugin.LooperPluginProvider;
import org.freshmarker.core.plugin.NumberPluginProvider;
import org.freshmarker.core.plugin.SequencePluginProvider;
import org.freshmarker.core.plugin.StringPluginProvider;
import org.freshmarker.core.plugin.SystemPluginProvider;
import org.freshmarker.core.plugin.TemporalPluginProvider;

import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Map;
import java.util.stream.Stream;

public class SourceCodeGenerator {

    private final Template template;

    public SourceCodeGenerator() {
        Configuration configuration = new Configuration();
        Stream.of(new StringPluginProvider(), new BooleanPluginProvider(), new NumberPluginProvider(), new TemporalPluginProvider(), new DatePluginProvider(),
                new LooperPluginProvider(), new EnumPluginProvider(), new SequencePluginProvider(), new SystemPluginProvider())
                .forEach(configuration::registerPlugin);
        template = configuration.builder().getTemplate("hamcrest", new InputStreamReader(getClass().getResourceAsStream("/template.ftl")));
    }

    public void processTemplate(Map<String, Object> root, PrintWriter out) {
        template.process(root, out);
    }
}
