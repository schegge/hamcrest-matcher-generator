package de.schegge.processor.visitors;

import static java.util.stream.Collectors.toSet;

import java.util.Set;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeMirror;

public class TypesFromGenericTypeCollector extends AbstractTypeVisitor<Set<TypeMirror>, Void> {

  @Override
  public Set<TypeMirror> visitDeclared(DeclaredType t, Void aVoid) {
    Set<TypeMirror> typeMirrors = t.getTypeArguments().stream()
        .map(tm -> tm.accept(this, null)).flatMap(Set::stream).collect(toSet());
    typeMirrors.add(t);
    return typeMirrors;
  }
}
