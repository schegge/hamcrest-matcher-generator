package de.schegge.processor.visitors;

import javax.lang.model.type.TypeMirror;

public class AnnotationTypeValueVisitor extends AbstractAnnotationValueVisitor<TypeMirror, Void> {

  @Override
  public TypeMirror visitType(TypeMirror t, Void unused) {
    return t;
  }
}
