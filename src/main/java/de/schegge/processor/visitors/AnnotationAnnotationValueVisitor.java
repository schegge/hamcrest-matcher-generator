package de.schegge.processor.visitors;

import javax.lang.model.element.AnnotationMirror;

public class AnnotationAnnotationValueVisitor extends AbstractAnnotationValueVisitor<AnnotationMirror, Void> {

  @Override
  public AnnotationMirror visitAnnotation(AnnotationMirror a, Void aVoid) {
    return a;
  }
}
