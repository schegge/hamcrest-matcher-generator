package de.schegge.processor.visitors;

import static java.util.stream.Collectors.joining;

import java.util.Map;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.PrimitiveType;

public class DictionaryBasedTypeNameGenerator<P> extends AbstractTypeVisitor<String, P> {

  private final ProcessingEnvironment processingEnv;
  private final Map<String, String> dictionary;

  public DictionaryBasedTypeNameGenerator(ProcessingEnvironment processingEnv, Map<String, String> dictionary) {
    this.processingEnv = processingEnv;
    this.dictionary = dictionary;
  }

  @Override
  public String visitPrimitive(PrimitiveType t, P parameter) {
    return processingEnv.getTypeUtils().boxedClass(t).getSimpleName().toString();
  }

  @Override
  public String visitDeclared(DeclaredType t, P parameter) {
    String genericPart = t.getTypeArguments().stream()
        .map(tm -> tm.accept(this, parameter)).collect(joining(","));
    String erasure = processingEnv.getTypeUtils().erasure(t).toString();
    String baseTypeName = dictionary.get(erasure);
    return genericPart.isEmpty() ? baseTypeName : baseTypeName + "<" + genericPart + ">";
  }
}
