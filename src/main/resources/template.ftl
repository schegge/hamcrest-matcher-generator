/**
* @author Hamcrest Matcher Generator by Jens Kaiser
* @version 1.0
*
* @see <a href="https://gitlab.com/schegge/hamcrest-matcher-generator">Hamcrest Matcher Generator</a>
*/
package ${package};

import static org.hamcrest.CoreMatchers.equalTo;

import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;
import de.schegge.hamcrest.OptionalMatchers;

<#list imports as import>
import ${import};
</#list>

public final class ${matcherTypeName} {
<#list methods as method>
  <#switch method.template>
    <#case "class.optionalMatcherMethod">

  public static Matcher<${sourceType}> hasOptional${method.attributeName}(final Matcher<${method.returnType}> ${method.variableName}Matcher) {
    return new FeatureMatcher<${sourceType}, ${method.returnType}>(${method.variableName}Matcher, "${method.variableName}", "${method.variableName}") {
      @Override
      protected ${method.returnType} featureValueOf(final ${sourceType} actual) {
        return actual.${method.methodName}();
      }
    };
  }
    <#case "class.optionalMethod">

  public static Matcher<${sourceType}> has${method.attributeName}(final Matcher<${method.returnType}> ${method.variableName}Matcher) {
    return hasOptional${method.attributeName}(OptionalMatchers.hasValue(${method.variableName}Matcher));
  }

  public static Matcher<${sourceType}> has${method.attributeName}(final ${method.returnType} ${method.variableName}) {
    return hasOptional${method.attributeName}(OptionalMatchers.hasValue(${method.variableName}));
  }

  public static Matcher<${sourceType}> hasEmpty${method.attributeName}() {
    return hasOptional${method.attributeName}(OptionalMatchers.isEmpty());
  }

  public static Matcher <${sourceType}> has${method.attributeName}() {
    return hasOptional${method.attributeName}(OptionalMatchers.isPresent());
  }
    <#case "class.method">

  public static Matcher<${sourceType}> has${method.attributeName}(final Matcher<${method.returnType}> ${method.variableName}Matcher) {
    return new FeatureMatcher<${sourceType}, ${method.returnType}>(${method.variableName}Matcher, "${method.variableName}", "${method.variableName}") {
      @Override
      protected ${method.returnType} featureValueOf(final ${sourceType} actual) {
        return actual.${method.methodName}();
      }
    };
  }

  public static Matcher<${sourceType}> has${method.attributeName}(final ${method.returnType} ${method.variableName}) {
    return has${method.attributeName}(equalTo(${method.variableName}));
  }
  </#switch>
</#list>
}